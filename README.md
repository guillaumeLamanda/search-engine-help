# Aide sur projet

## Commencer

- `git clone <repoUrl>`
- `yarn`: installation des paquets
  - solution alternative : `npm install`
- `yarn dev` lancement du serveur de développement

Ouvrir un navigateur à l'url http://localhost:8082
