document.getElementById("search").onkeypress = function(event) {
  if (event.keyCode == 13 || event.which == 13) {
    doSearch(document.getElementById("search").value)
  }
}

async function doSearch(words) {
  console.log(words)
  if (words.length !== 0) {
    // alert("vous avez tapé : " + words)
    const datas = await sendQuery(words)
    datas.map(element => {
      var c = document.createElement("li")
      c.append(element.text)
      document.getElementById("results").appendChild(c)
    })
  } else {
    alert("Vous n'avez rien saisi...")
  }
}

/*-------------- QUERY FUNCTION -------------------*/
async function sendQuery(words) {
  let query = "http://localhost:8082/search?search=" + words
  let myHeaders = new Headers({
    "content-type": "application/json"
  })
  let myRequest = new Request(query)
  let myInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  }

  // Miss a return statement
  return fetch(myRequest, myInit).then(res => {
    if (res.ok) {
      return res.json()
    } else throw new Error(res.statusText)
  })
}
