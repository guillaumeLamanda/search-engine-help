const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const { join } = require("path")
const logger = require("morgan")

const app = express()

app.use(bodyParser.json())
app.use(logger("dev"))

const fakeDatas = [{ text: "hello" }, { text: "word" }]

// Define static routes to project ressources
app.use(express.static(join(__dirname, "public")))

// Allow cors
app.use(cors())

// Load front page
app.get("/search", (req, res) => {
  return res.json(fakeDatas)
})

app.listen(8082)
